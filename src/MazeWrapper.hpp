#pragma once

#include <memory>
#include <vector>

#include "LocationData.hpp"
#include "Tile.hpp"

/**
 * @brief A class representing a wrapper for a maze.
 */
class MazeWrapper {
   private:
    std::vector<std::vector<std::shared_ptr<Tile>>> mMazeData;  ///< The data for the maze.

   public:
    /**
     * @brief Constructs a MazeWrapper object with the given maze data.
     * @param mazeData The data for the maze.
     */
    MazeWrapper(std::vector<std::vector<std::shared_ptr<Tile>>> mazeData);
    /**
     * @brief Checks if the given location is within the bounds of the maze.
     * @param location The location to check.
     * @return true if the location is within the bounds of the maze, false otherwise.
     */
    bool checkLocation(LocationData location) const;
    /**
     * @brief Checks if the player can go to the given location in the maze.
     * @param location The location to check.
     * @return true if the player can go to the given location, false otherwise.
     */
    bool canGoHere(LocationData location) const;
    /**
     * @brief Checks if the tile at the given location can be destroyed.
     * @param location The location to check.
     * @return true if the tile at the given location can be destroyed, false otherwise.
     */
    bool canDestroy(LocationData location) const;
    /**
     * @brief Destroys the tile at the given location in the maze.
     * @param location The location of the tile to destroy.
     * @return true if the tile was successfully destroyed, false otherwise.
     */
    bool destroy(LocationData location);
    /**
     * @brief Renders the maze on the screen.
     */
    void render() const;
};