#pragma once
#include <exception>
#include <string>
/**
 * @brief A class representing an exception thrown when input data is malformed.
 */
class MalformedDataException : public std::exception {
   private:
    std::string mMessage;  ///< The message associated with the exception.

   public:
    /**
     * @brief Constructs a MalformedDataException object with a default message.
     */
    MalformedDataException() : mMessage("Input file is not in correct form") {}
    /**
     * @brief Constructs a MalformedDataException object with the given message.
     * @param message The message associated with the exception.
     */
    MalformedDataException(std::string message) : mMessage(message) {}
    /**
     * @brief Gets the message associated with the exception.
     * @return The message associated with the exception.
     */
    const char* what() const noexcept override { return mMessage.c_str(); }
};