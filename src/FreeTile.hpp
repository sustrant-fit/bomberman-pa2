#pragma once
#include "Tile.hpp"
/**
 * @brief A class representing a free tile on a game board.
 */
class FreeTile : public Tile {
   public:
    /**
     * @brief Constructs a FreeTile object.
     */
    FreeTile();
    /**
     * @brief Checks if the free tile is walkable.
     * @return true since free tiles are walkable.
     */
    bool isWalkable() const override;
    /**
     * @brief Checks if the tile is destructable.
     * @return false since free tiles are not destructable.
     */
    bool isDestructable() const override;
};