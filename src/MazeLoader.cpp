#include "MazeLoader.hpp"

#include <fstream>

#include "AiPlayer.hpp"
#include "DestructableWall.hpp"
#include "FreeTile.hpp"
#include "HumanPlayer.hpp"
#include "MalformedDataException.hpp"
#include "Wall.hpp"

MazeLoader::MazeLoader() : validData(false) {}

void MazeLoader::loadData(const std::string &fileName) {
    std::ifstream inStream(fileName, std::ifstream::in);
    if (!inStream.is_open()) {
        throw MalformedDataException("Failed to open maze file");
    }

    int numOfColumns;
    int numOfRows;

    inStream >> numOfColumns;
    if (!inStream.good() || numOfColumns < 0 || numOfColumns > 50) {
        throw MalformedDataException("Failed to load columns");
    }
    inStream >> numOfRows;
    if (!inStream.good() || numOfRows < 0 || numOfRows > 50) {
        throw MalformedDataException("Failed to load rows");
    }
    for (int i = 0; i < numOfRows; i++) {
        std::vector<char> loadingRow;
        char loadedChar;
        for (int j = 0; j < numOfColumns; j++) {
            inStream >> loadedChar;
            if (!inStream.good() || (loadedChar != '#' && loadedChar != '.' && loadedChar != '+')) {
                throw MalformedDataException("Invalid maze character");
            }
            loadingRow.push_back(loadedChar);
        }
        rawMazeData.push_back(loadingRow);
    }
    for (int i = 0; i < numOfColumns; i++) {
        if (rawMazeData[0][i] != '#') {
            throw MalformedDataException("Maze not surrounded by wall");
        }
        if (rawMazeData[numOfRows - 1][i] != '#') {
            throw MalformedDataException("Maze not surrounded by wall");
        }
    }
    for (int i = 0; i < numOfRows; i++) {
        if (rawMazeData[i][0] != '#') {
            throw MalformedDataException("Maze not surrounded by wall");
        }
        if (rawMazeData[i][numOfColumns - 1] != '#') {
            throw MalformedDataException("Maze not surrounded by wall");
        }
    }

    int bombTimerInSeconds;
    int chanceOfLiveIncrese;
    int chanceOfBombDamageIncrese;
    int chanceOfWalkSpeedIncrese;
    int chanceOfBombNumberIncrese;
    int chanceOfBombExplosionSizeIncrese;

    inStream >> bombTimerInSeconds;
    if (!inStream.good() || bombTimerInSeconds < 0) {
        throw MalformedDataException("Failed to load bomb timer");
    }
    inStream >> chanceOfLiveIncrese;
    if (!inStream.good() || chanceOfLiveIncrese < 0 || chanceOfLiveIncrese > 100) {
        throw MalformedDataException("Failed to load live increse");
    }
    inStream >> chanceOfBombDamageIncrese;
    if (!inStream.good() || chanceOfBombDamageIncrese < 0 || chanceOfBombDamageIncrese > 100) {
        throw MalformedDataException("Failed to load bomb damage increse");
    }
    inStream >> chanceOfWalkSpeedIncrese;
    if (!inStream.good() || chanceOfWalkSpeedIncrese < 0 || chanceOfWalkSpeedIncrese > 100) {
        throw MalformedDataException("Failed to load walk speed increse");
    }
    inStream >> chanceOfBombNumberIncrese;
    if (!inStream.good() || chanceOfBombNumberIncrese < 0 || chanceOfBombNumberIncrese > 100) {
        throw MalformedDataException("Failed to load bomb number increse");
    }
    inStream >> chanceOfBombExplosionSizeIncrese;
    if (!inStream.good() || chanceOfBombExplosionSizeIncrese < 0 || chanceOfBombExplosionSizeIncrese > 100) {
        throw MalformedDataException("Failed to load explosion size increse");
    }
    if (chanceOfLiveIncrese + chanceOfBombDamageIncrese + chanceOfWalkSpeedIncrese + chanceOfBombNumberIncrese + chanceOfBombExplosionSizeIncrese >
        100) {
        throw MalformedDataException("Chances are higher then 100 %");
    }
    mConstants = GameConstants(numOfColumns, numOfRows, bombTimerInSeconds, chanceOfLiveIncrese, chanceOfBombDamageIncrese, chanceOfWalkSpeedIncrese,
                               chanceOfBombNumberIncrese, chanceOfBombExplosionSizeIncrese);

    while (!inStream.eof()) {
        std::string name;
        char loadedPlayerType;
        int playerColumn;
        int playerRow;

        inStream >> name;
        if (!inStream.good() || name.empty()) {
            throw MalformedDataException("Failed to load player name");
        }
        inStream >> loadedPlayerType;
        if (!inStream.good() || (loadedPlayerType != 'A' && loadedPlayerType != 'H')) {
            throw MalformedDataException("Failed to load player type");
        }
        inStream >> playerColumn;
        if (!inStream.good() || playerColumn < 0 || playerColumn > numOfColumns) {
            throw MalformedDataException("Failed to load player column");
        }
        inStream >> playerRow;
        if (inStream.bad() || (inStream.eof() && loadedPlayerType == 'H') || playerRow < 0 || playerRow > numOfRows) {
            throw MalformedDataException("Failed to load player row");
        }
        if (loadedPlayerType == 'H') {
            std::string controlls[5];
            for (auto &controll : controlls) {
                inStream >> controll;
                if (inStream.bad() || controll.size() > 1 || !isalnum(controll[0])) {
                    throw MalformedDataException("Failed to load human player controlls");
                }
            }
            for (size_t i = 0; i < 5; i++) {
                for (size_t j = 0; j < 5; j++) {
                    if (i != j) {
                        if (controlls[i] == controlls[j]) {
                            throw MalformedDataException("Duplicate key in human player controlls");
                        }
                    }
                }
            }

            KeyboardControls keyboardControlls(controlls[0], controlls[1], controlls[2], controlls[3], controlls[4]);
            humanPlayers.emplace_back(keyboardControlls, playerColumn, playerRow, name);
        } else {
            AiPlayers.emplace_back(playerColumn, playerRow, name);
        }
    }

    validData = true;
}

void MazeLoader::checkValid() const {
    if (!validData) {
        throw MalformedDataException("Data not loaded yet");
    }
}

bool MazeLoader::registerPlayers(Game &game) const {
    checkValid();
    for (auto &&i : humanPlayers) {
        game.registerPlayer(std::make_shared<HumanPlayer>(game.getInputHandler(), i.mControlls, game, LocationData(i.mColumn, i.mRow), i.mName));
    }
    for (auto &&i : AiPlayers) {
        game.registerPlayer(std::make_shared<AIPlayer>(game, LocationData(i.mColumn, i.mRow), i.mName));
    }
    return true;
}

MazeWrapper MazeLoader::buildMaze() const {
    checkValid();
    std::vector<std::vector<std::shared_ptr<Tile>>> newMazeData;
    std::shared_ptr<Wall> wall = std::make_shared<Wall>();
    std::shared_ptr<FreeTile> movingSpace = std::make_shared<FreeTile>();
    std::shared_ptr<DestructableWall> destructableWall = std::make_shared<DestructableWall>();

    for (int i = 0; i < mConstants.getNumOfRows(); i++) {
        std::vector<std::shared_ptr<Tile>> loadingRow;
        for (int j = 0; j < mConstants.getNumOfColumns(); j++) {
            switch (rawMazeData[i][j]) {
                case '#':
                    loadingRow.push_back(wall);
                    break;
                case '+':
                    loadingRow.push_back(destructableWall);
                    break;
                case '.':
                    loadingRow.push_back(movingSpace);
                    break;

                default:
                    throw MalformedDataException("Invalid maze character was loaded before");
            }
        }
        newMazeData.push_back(loadingRow);
    }
    return MazeWrapper(newMazeData);
}

GameConstants MazeLoader::getConstants() const { return mConstants; }