#pragma once
#include <string>
/**
 * @brief A class representing the keyboard controls for a player in a game.
 */
class KeyboardControls {
   public:
    /**
     * @brief Constructs a KeyboardControls object with the given key bindings.
     * @param moveUp The key binding for moving up.
     * @param moveDown The key binding for moving down.
     * @param moveLeft The key binding for moving left.
     * @param moveRight The key binding for moving right.
     * @param placeBomb The key binding for placing a bomb.
     */
    KeyboardControls(std::string moveUp, std::string moveDown, std::string moveLeft, std::string moveRight, std::string placeBomb)
        : mMoveDown(moveDown), mMoveLeft(moveLeft), mMoveRight(moveRight), mMoveUp(moveUp), mPlaceBomb(placeBomb) {}
    std::string mMoveDown;   ///< The key binding for moving down.
    std::string mMoveLeft;   ///< The key binding for moving left.
    std::string mMoveRight;  ///< The key binding for moving right.
    std::string mMoveUp;     ///< The key binding for moving up.
    std::string mPlaceBomb;  ///< The key binding for placing a bomb.
};
