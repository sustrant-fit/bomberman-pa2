#include "IncreseExplosionSize.hpp"

IncreseExplosionSize::IncreseExplosionSize(LocationData location, Game& game) : PowerUp(location, "E", game) {}

void IncreseExplosionSize::applyEfect(PlayerStats& player) const { player.mBombExplosionSize++; }