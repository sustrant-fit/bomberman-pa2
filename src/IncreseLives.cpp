#include "IncreseLives.hpp"

IncreseLives::IncreseLives(LocationData location, Game& game) : PowerUp(location, "H", game) {}

void IncreseLives::applyEfect(PlayerStats& player) const { player.mNumOfLives++; }