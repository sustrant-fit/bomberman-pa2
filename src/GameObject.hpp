
#pragma once

#include <string>

#include "LocationData.hpp"
#include "PlayerStats.hpp"
/**
 * @brief A class representing a game object in a game.
 */
class GameObject {
    std::string mPrintData;  ///< The text representation of the game object.

   protected:
    LocationData mLocation;  ///< The location of the game object on the game board.

   public:
    /**
     * @brief Constructs a GameObject object with the given print data and location.
     * @param printData The text representation of the game object.
     * @param location The location of the game object on the game board.
     */
    GameObject(std::string printData, LocationData location) : mPrintData(printData), mLocation(location) {}
    GameObject(const GameObject&) = delete;
    GameObject& operator=(const GameObject&) = delete;
    virtual ~GameObject() = default;
    /**
     * @brief Draws the game object on the screen.
     */
    virtual void draw() const;
    /**
     * @brief Updates the state of the game object.
     */
    virtual void update();
    /**
     * @brief Gets the location of the game object on the game board.
     * @return The location of the game object on the game board.
     */
    LocationData getLocation() const;
    /**
     * @brief Checks for collision between the game object and a player at the given location.
     * @param location The location of the player on the game board.
     * @param player The player to check for collision with.
     */
    virtual void checkCollisonWithPlayer(LocationData location, PlayerStats& player);
};
