#include "HumanPlayer.hpp"

HumanPlayer::HumanPlayer(InputHandler& inputHandler, KeyboardControls controlls, Game& game, LocationData location, const std::string& name)
    : Player(game, location, name) {
    inputHandler.registerInput(
        name, controlls, [&]() { this->moveUp(); }, [&]() { this->moveDown(); }, [&]() { this->moveLeft(); }, [&]() { this->moveRight(); },
        [&]() { this->placeBomb(); });
}