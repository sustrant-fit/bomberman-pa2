#include "Player.hpp"

#include "Bomb.hpp"

Player::Player(Game& game, LocationData location, std::string name) : GameObject("&", location), mGame(game), mName(name) {}

bool Player::tryMove(LocationData direction) {
    LocationData startingLocation = mLocation;
    for (int i = 0; i < mStats.mWalkSpeedMultiplier; i++) {
        LocationData futureLocation = mLocation;
        futureLocation.mColumn += direction.mColumn;
        futureLocation.mRow += direction.mRow;
        if (mGame.getMaze().canGoHere(futureLocation)) {
            mLocation.mColumn += direction.mColumn;
            mLocation.mRow += direction.mRow;
        } else {
            break;
        }
    }
    if (startingLocation != mLocation) {
        return true;
    } else {
        return false;
    }
}

bool Player::moveLeft() { return tryMove(LocationData(-1, 0)); }
bool Player::moveRight() { return tryMove(LocationData(1, 0)); }
bool Player::moveUp() { return tryMove(LocationData(0, -1)); }
bool Player::moveDown() { return tryMove(LocationData(0, 1)); }

bool Player::placeBomb() {
    if (mStats.mNumOfAvaliableBombs <= mNumOfPlacedBombs) {
        return false;
    }
    mGame.registerGameObject(std::make_shared<Bomb>(mGame, mLocation, mStats.mBombExplosionSize, mStats.mBombDamage));
    mNumOfPlacedBombs++;
    bombTimers.push_back(mGame.getGameConstants().getBombTimerInSeconds());
    return true;
}

PlayerStats& Player::getStats() { return mStats; }

std::string Player::getName() const { return mName; }

void Player::returnBomb() {
    if (mNumOfPlacedBombs > 0) {
        mNumOfPlacedBombs--;
    }
}

void Player::update() {
    if (mStats.mNumOfLives < 1) {
        mGame.removePlayer(shared_from_this());
        return;
    }

    mStats.mScore++;

    for (size_t i = 0; i < bombTimers.size(); ++i) {
        bombTimers.at(i)--;
        if (bombTimers.at(i) == 0) {
            returnBomb();
            bombTimers.pop_front();
        }
    }
}
