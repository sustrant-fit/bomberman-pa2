#include "GameConstants.hpp"

#include "MalformedDataException.hpp"

GameConstants::GameConstants() : validData(false) {}

GameConstants::GameConstants(int numOfColumns, int numOfRows, int bombTimerInSeconds, int chanceOfLiveIncrese, int chanceOfBombDamageIncrese,
                             int chanceOfWalkSpeedIncrese, int chanceOfBombNumberIncrese, int chanceOfBombExplosionSizeIncrese)
    : validData(true),
      mNumOfColumns(numOfColumns),
      mNumOfRows(numOfRows),
      mBombTimerInSeconds(bombTimerInSeconds),
      mChanceOfLiveIncrese(chanceOfLiveIncrese),
      mChanceOfBombDamageIncrese(chanceOfBombDamageIncrese),
      mChanceOfWalkSpeedIncrese(chanceOfWalkSpeedIncrese),
      mChanceOfBombNumberIncrese(chanceOfBombNumberIncrese),
      mChanceOfBombExplosionSizeIncrese(chanceOfBombExplosionSizeIncrese) {}

void GameConstants::checkValid() const {
    if (!validData) {
        throw MalformedDataException();
    }
}

int GameConstants::getNumOfColumns() const {
    checkValid();
    return mNumOfColumns;
}
int GameConstants::getNumOfRows() const {
    checkValid();
    return mNumOfRows;
}
int GameConstants::getBombTimerInSeconds() const {
    checkValid();
    return mBombTimerInSeconds;
}
int GameConstants::getChanceOfLiveIncrese() const {
    checkValid();
    return mChanceOfLiveIncrese;
}
int GameConstants::getChanceOfBombDamageIncrese() const {
    checkValid();
    return mChanceOfBombDamageIncrese;
}
int GameConstants::getChanceOfWalkSpeedIncrese() const {
    checkValid();
    return mChanceOfWalkSpeedIncrese;
}
int GameConstants::getChanceOfBombNumberIncrese() const {
    checkValid();
    return mChanceOfBombNumberIncrese;
}
int GameConstants::getChanceOfBombExplosionSizeIncrese() const {
    checkValid();
    return mChanceOfBombExplosionSizeIncrese;
}