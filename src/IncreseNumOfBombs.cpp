#include "IncreseNumOfBombs.hpp"

IncreseNumOfBombs::IncreseNumOfBombs(LocationData location, Game& game) : PowerUp(location, "b", game) {}

void IncreseNumOfBombs::applyEfect(PlayerStats& player) const { player.mNumOfAvaliableBombs++; }