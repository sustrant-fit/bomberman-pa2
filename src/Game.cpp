#include "Game.hpp"

#include <iostream>

#include "NoPlayersException.hpp"
#include "Player.hpp"

Game::Game(MazeWrapper maze, GameConstants constants, ScoreTable& scoreTable) : mMaze(maze), mConstants(constants), mScoreTable(scoreTable) {}

void Game::render() const {
    mMaze.render();
    for (auto&& it = mGameObjects.cbegin(); it != mGameObjects.cend();) {
        (*it++)->draw();
    }
}

MazeWrapper& Game::getMaze() { return mMaze; }

GameConstants& Game::getGameConstants() { return mConstants; }

InputHandler& Game::getInputHandler() { return mInputHandler; }

void Game::registerGameObject(const std::shared_ptr<GameObject>& toRegister) { mGameObjects.emplace(toRegister); }

void Game::registerPlayer(const std::shared_ptr<Player>& toRegister) {
    mPlayers.emplace(toRegister);
    mGameObjects.emplace(toRegister);
}

void Game::removeObject(const std::shared_ptr<GameObject>& toRemove) { mGameObjects.erase(toRemove); }

void Game::removePlayer(const std::shared_ptr<Player>& toRemove) {
    mScoreTable.insertPlayer(toRemove->getName(), toRemove->getStats().mScore);
    mInputHandler.tryRemoveInput(toRemove->getName());
    mGameObjects.erase(toRemove);
    mPlayers.erase(toRemove);
    if (mPlayers.empty()) {
        throw NoPlayersException();
    }
}

void Game::update() {
    for (auto&& it = mGameObjects.cbegin(); it != mGameObjects.cend();) {
        (*it++)->update();
    }
    if (mPlayers.size() < 2) {
        mScoreTable.insertPlayer(mPlayers.begin()->get()->getName(), mPlayers.begin()->get()->getStats().mScore);
        throw NoPlayersException(mPlayers.begin()->get()->getName() + " is last standing, " + mPlayers.begin()->get()->getName() + " won");
    }
    processColisions();
}

void Game::run() {
    while (true) {
        try {
            render();
            mInputHandler.handleInput(std::cin, LocationData(0, mConstants.getNumOfRows()));
            update();
        } catch (const NoPlayersException& e) {
            std::cout << "\u001b[2J"
                      << "\u001b[H" << e.what() << '\n';
            std::cout << "Scores: \n";
            mScoreTable.displayScoreTable();
            break;
        } catch (const std::exception& e) {
            std::cout << "\u001b[2J"
                      << "\u001b[H";
            std::cerr << e.what() << '\n';
        }
    }
}

void Game::processColisions() {
    for (auto&& i : mPlayers) {
        for (auto&& j : mGameObjects) {
            j->checkCollisonWithPlayer(i->getLocation(), i->getStats());
        }
    }
}