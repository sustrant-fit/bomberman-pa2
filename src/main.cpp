#include <iostream>

#include "App.hpp"

int main(int argc, char* argv[]) {
    if (argc != 3 || argv[1] == nullptr || argv[2] == nullptr) {
        std::cout << "Invalid arguments (Arguments are: Maze file and score table file)" << std::endl;
        return -1;
    }

    App app;
    return app.run(argv[1], argv[2]);
}
