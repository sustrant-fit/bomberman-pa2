#pragma once
#include "Tile.hpp"

/**
 * @brief A class representing a wall tile on a game board.
 */
class Wall : public Tile {
   public:
    /**
     * @brief Constructs a Wall object.
     */
    Wall();
    /**
     * @brief Checks if the wall tile is walkable.
     * @return false since wall tiles are not walkable.
     */
    bool isWalkable() const override;
    /**
     * @brief Checks if the wall tile is destructable.
     * @return false since wall tiles are not destructable.
     */
    bool isDestructable() const override;
};