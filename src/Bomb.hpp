#pragma once
#include "Game.hpp"
#include "GameObject.hpp"
#include "PlayerStats.hpp"
/**
 * @brief A class representing a bomb in a game.
 */
class Bomb : public GameObject, public std::enable_shared_from_this<Bomb> {
   private:
    bool mExploded;                ///< Flag indicating if the bomb has exploded.
    Game& mGame;                   ///< Reference to the game object.
    unsigned mTimeUntilExplosion;  ///< The time until the bomb explodes.
    int mDamage;                   ///< The damage caused by the bomb explosion.
    int mExplosionSize;            ///< The size of the bomb explosion.

   public:
    /**
     * @brief Constructs a Bomb object with the given game reference, location, explosion size, and damage.
     * @param game Reference to the game object.
     * @param location The location of the bomb on the game board.
     * @param explosionSize The size of the bomb explosion.
     * @param damage The damage caused by the bomb explosion.
     */
    Bomb(Game& game, LocationData location, int explosionSize, int damage);
    /**
     * @brief Updates the state of the bomb and its timer.
     */
    void update() override;
    /**
     * @brief Draws bomb explosion.
     */
    void explode() const;
    /**
     * @brief Draws the bomb on the screen.
     */
    void draw() const override;
    /**
     * @brief Checks for collision between the bomb explosion and a player at the given location.
     * @param playerLocation The location of the player on the game board.
     * @param player The player to check for collision with.
     */
    void checkCollisonWithPlayer(LocationData playerLocation, PlayerStats& player) override;
};
