#pragma once
/**
 * @brief A class representing the stats of a player in a game.
 */
class PlayerStats {
   public:
    int mNumOfAvaliableBombs = 1;  ///< The number of available bombs for the player.
    int mBombExplosionSize = 1;    ///< The size of the bomb explosion for the player.
    int mBombDamage = 1;           ///< The damage caused by the player's bombs.
    int mNumOfLives = 1;           ///< The number of lives for the player.
    int mWalkSpeedMultiplier = 1;  ///< The walk speed multiplier for the player.
    int mScore = 0;                ///< The score of the player.
};