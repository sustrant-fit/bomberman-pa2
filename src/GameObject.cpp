#include "GameObject.hpp"

#include <iostream>

void GameObject::draw() const {
    std::cout << "\u001b[" << mLocation.mRow << ";" << mLocation.mColumn << "H";

    std::cout << mPrintData;
    std::cout << "\u001b[0m";
}

void GameObject::update() { return; }

void GameObject::checkCollisonWithPlayer(LocationData location, PlayerStats& player) { return; }

LocationData GameObject::getLocation() const { return mLocation; }