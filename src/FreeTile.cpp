#include "FreeTile.hpp"

FreeTile::FreeTile() : Tile(" ") {}

bool FreeTile::isWalkable() const { return true; }
bool FreeTile::isDestructable() const { return false; }
