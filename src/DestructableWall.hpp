#pragma once
#include "Tile.hpp"
/**
 * @brief A class representing a destructable wall tile on a game board.
 */
class DestructableWall : public Tile {
   public:
    /**
     * @brief Constructs a DestructableWall object.
     */
    DestructableWall();
    /**
     * @brief Checks if the wall tile is walkable.
     * @return false since wall tiles are not walkable.
     */
    bool isWalkable() const override;
    /**
     * @brief Checks if the destructable wall tile is destructable.
     * @return true since destructable wall tiles are destructable.
     */
    bool isDestructable() const override;
};