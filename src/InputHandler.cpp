#include "InputHandler.hpp"

#include <unistd.h>

#include "NoPlayersException.hpp"

void InputHandler::registerInput(std::string playerName, KeyboardControls keys, std::function<void()> upFunctionToCall,
                                 std::function<void()> downFunctionToCall, std::function<void()> leftFunctionToCall,
                                 std::function<void()> rightFunctionToCall, std::function<void()> placeBombFunctionToCall) {
    std::map<std::string, std::function<void()>> playerHandler;
    playerHandler.emplace(keys.mMoveUp, upFunctionToCall);
    playerHandler.emplace(keys.mMoveDown, downFunctionToCall);
    playerHandler.emplace(keys.mMoveLeft, leftFunctionToCall);
    playerHandler.emplace(keys.mMoveRight, rightFunctionToCall);
    playerHandler.emplace(keys.mPlaceBomb, placeBombFunctionToCall);
    keyBindings.emplace(playerName, playerHandler);
}
void InputHandler::handleInput(std::istream& input, LocationData inputLocation) const {
    std::cout << "\u001b[" << inputLocation.mRow << ";" << inputLocation.mColumn << "H" << std::endl;
    usleep(300000);
    if (!input.good()) {
        sleep(1);
    }
    if (keyBindings.empty()) {
        throw NoPlayersException("No more human players, ending game");
    }

    for (auto&& playerBinds : keyBindings) {
        std::cout << playerBinds.first << " enter next move: " << std::endl;
        std::string data;
        input >> data;
        auto found = playerBinds.second.find(data);
        if (found == playerBinds.second.end()) {
            std::cout << "Invalid key, skipping" << std::endl;
        } else {
            found->second();
        }
    }
}
void InputHandler::tryRemoveInput(const std::string& playerName) { keyBindings.erase(playerName); }