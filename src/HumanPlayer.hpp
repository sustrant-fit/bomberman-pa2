#pragma once
#include "InputHandler.hpp"
#include "KeyboardControls.hpp"
#include "Player.hpp"
/**
 * @brief A class representing a human player in a game.
 */
class HumanPlayer : public Player {
   public:
    /**
     * @brief Constructs a HumanPlayer object with the given input handler, controls, game reference, location, and name. And registers controls to
     * input handler
     * @param inputHandler The input handler for the player.
     * @param controls The keyboard controls for the player.
     * @param game Reference to the game object.
     * @param location The starting location of the player on the game board.
     * @param name The name of the player.
     */
    HumanPlayer(InputHandler& inputHandler, KeyboardControls controlls, Game& game, LocationData location, const std::string& name);
};
