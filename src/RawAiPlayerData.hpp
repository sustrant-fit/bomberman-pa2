#pragma once
#include <string>
/**
 * @brief A class representing raw data for a AI player.
 */
class RawAiPlayerData {
   public:
    /**
     * @brief Constructs a RawAiPlayerData object with the give, column, row, and name.
     * @param column The starting column for the player.
     * @param row The starting row for the player.
     * @param name The name of the player.
     */
    RawAiPlayerData(int column, int row, std::string name) : mColumn(column), mRow(row), mName(name) {}
    int mColumn;        ///< The starting column for the player.
    int mRow;           ///< The starting row for the player.
    std::string mName;  ///< The name of the player.
};