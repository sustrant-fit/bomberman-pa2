#pragma once
#include "Player.hpp"
/**
 * @brief A class representing a AI player in a game.
 */
class AIPlayer : public Player {
    unsigned lastDirection;

   public:
    /**
     * @brief Constructs a AIPlayer object with the given input handler, controls, game reference, location, and name. And registers controls to
     * input handler
     * @param inputHandler The input handler for the player.
     * @param controls The keyboard controls for the player.
     * @param game Reference to the game object.
     * @param location The starting location of the player on the game board.
     * @param name The name of the player.
     */
    AIPlayer(Game& game, LocationData location, std::string name);
    void update() override;
};