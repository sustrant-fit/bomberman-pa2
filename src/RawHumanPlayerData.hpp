#pragma once
#include "KeyboardControls.hpp"
/**
 * @brief A class representing raw data for a human player.
 */
class RawHumanPlayerData {
   public:
    /**
     * @brief Constructs a RawHumanPlayerData object with the given controls, column, row, and name.
     * @param controls The keyboard controls for the player.
     * @param column The starting column for the player.
     * @param row The starting row for the player.
     * @param name The name of the player.
     */
    RawHumanPlayerData(KeyboardControls controlls, int column, int row, std::string name)
        : mColumn(column), mRow(row), mName(name), mControlls(controlls) {}
    int mColumn;                  ///< The starting column for the player.
    int mRow;                     ///< The starting row for the player.
    std::string mName;            ///< The name of the player.
    KeyboardControls mControlls;  ///< The keyboard controls for the player.
};