#pragma once
#include <string>

#include "Game.hpp"
#include "GameObject.hpp"
#include "LocationData.hpp"
#include "PlayerStats.hpp"
/**
 * @brief A class representing a power-up in a game.
 */
class PowerUp : public GameObject, public std::enable_shared_from_this<PowerUp> {
   private:
    Game& mGame;  ///< Reference to the game object.

   public:
    /**
     * @brief Constructs a PowerUp object with the given location, print data, and game reference.
     * @param location The location of the power-up on the game board.
     * @param printData The text representation of the power-up.
     * @param game Reference to the game object.
     */
    PowerUp(LocationData location, std::string printData, Game& game) : GameObject(printData, location), mGame(game) {}
    /**
     * @brief Applies the effect of the power-up to the given player.
     * @param player The player to apply the effect to.
     */
    virtual void applyEfect(PlayerStats& player) const = 0;
    /**
     * @brief Checks for collision between the power-up and the player.
     * @param location The location of the player on the game board.
     * @param player The player to check for collision with.
     */
    void checkCollisonWithPlayer(LocationData location, PlayerStats& player) override;
};