#include "AiPlayer.hpp"

AIPlayer::AIPlayer(Game& game, LocationData location, std::string name) : Player(game, location, name), lastDirection(3) {}
/**
 * left - 1
 * right - 2
 * up - 3
 * down - 4
 */
void AIPlayer::update() {
    switch (lastDirection) {
        case 1:
            if (!moveLeft()) {
                if (!moveUp()) {
                    if (!moveDown()) {
                        if (mNumOfPlacedBombs > 0) {
                            break;
                        }
                        placeBomb();
                        lastDirection = 2;
                        break;
                    }
                    lastDirection = 4;
                    break;
                }
                lastDirection = 3;
            }
            break;
        case 2:
            if (!moveRight()) {
                if (!moveUp()) {
                    if (!moveDown()) {
                        if (mNumOfPlacedBombs > 0) {
                            break;
                        }
                        placeBomb();
                        lastDirection = 1;
                        break;
                    }
                    lastDirection = 4;
                    break;
                }
                lastDirection = 3;
            }
            break;
        case 3:
            if (!moveUp()) {
                if (!moveLeft()) {
                    if (!moveRight()) {
                        if (mNumOfPlacedBombs > 0) {
                            break;
                        }
                        placeBomb();
                        lastDirection = 4;
                        break;
                    }
                    lastDirection = 2;
                    break;
                }
                lastDirection = 1;
            }
            break;
        case 4:
            if (!moveDown()) {
                if (!moveLeft()) {
                    if (!moveRight()) {
                        if (mNumOfPlacedBombs > 0) {
                            break;
                        }
                        placeBomb();
                        lastDirection = 3;
                        break;
                    }
                    lastDirection = 2;
                    break;
                }
                lastDirection = 1;
            }
            break;
        default:
            break;
    }
    Player::update();
}