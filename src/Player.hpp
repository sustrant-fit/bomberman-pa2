#pragma once

#include <deque>
#include <iostream>
#include <string>

#include "Game.hpp"
#include "GameObject.hpp"
#include "LocationData.hpp"
#include "PlayerStats.hpp"
/**
 * @brief A class representing a player in a game.
 */
class Player : public GameObject, public std::enable_shared_from_this<Player> {
   private:
    PlayerStats mStats;               ///< The stats of the player.
    Game& mGame;                      ///< Reference to the game object.
    std::string mName;                ///< The name of the player.
    std::deque<unsigned> bombTimers;  ///< The timers for the player's bombs.
    /**
     * @brief Tries to move the player in the given direction.
     * @param direction The direction to move the player in.
     * @return true if the move was a bit successful, false otherwise.
     */
    bool tryMove(LocationData direction);
    /**
     * @brief Returns a bomb to the player's inventory.
     */
    void returnBomb();

   protected:
    int mNumOfPlacedBombs = 0;  ///< The number of bombs placed by the player.

   public:
    /**
     * @brief Constructs a Player object with the given game reference, location, and name.
     * @param game Reference to the game object.
     * @param location The starting location of the player on the game board.
     * @param name The name of the player.
     */
    Player(Game& game, LocationData location, std::string name);
    /**
     * @brief Moves the player to the right.
     * @return true if the move was successful, false otherwise.
     */
    bool moveRight();
    /**
     * @brief Moves the player to the left.
     * @return true if the move was successful, false otherwise.
     */
    bool moveLeft();
    /**
     * @brief Moves the player up.
     * @return true if the move was successful, false otherwise.
     */
    bool moveUp();
    /**
     * @brief Moves the player down.
     * @return true if the move was successful, false otherwise.
     */
    bool moveDown();
    /**
     * @brief Places a bomb at the player's current location.
     * @return true if a bomb was placed successfully, false otherwise.
     */
    bool placeBomb();
    /**
     * @brief Gets a reference to the player's stats.
     * @return A reference to the player's stats.
     */
    PlayerStats& getStats();
    /**
     * @brief Gets the name of the player.
     * @return The name of the player.
     */
    std::string getName() const;
    /**
     * @brief Updates the state of the player and its bombs.
     */
    void update() override;
};