#pragma once
#include <string>
#include <vector>

#include "Game.hpp"
#include "GameConstants.hpp"
#include "MazeWrapper.hpp"
#include "RawAiPlayerData.hpp"
#include "RawHumanPlayerData.hpp"

/**
 * @brief A class representing a loader for a maze.
 */
class MazeLoader {
   private:
    bool validData;  ///< Flag indicating if the maze data is valid.
    /**
     * @brief Checks if the maze data is valid.
     */
    void checkValid() const;
    GameConstants mConstants;                      ///< The game constants for the maze.
    std::vector<std::vector<char>> rawMazeData;    ///< The raw data for the maze.
    std::vector<RawHumanPlayerData> humanPlayers;  ///< The raw data for the human players in the maze.
    std::vector<RawAiPlayerData> AiPlayers;        ///< The raw data for the AI players in the maze.

   public:
    /**
     * @brief Constructs a MazeLoader object.
     */
    MazeLoader();
    /**
     * @brief Loads maze data from the given file.
     * @param fileName The name of the file to load data from.
     */
    void loadData(const std::string& fileName);
    /**
     * @brief Registers players in the given game using the loaded player data.
     * @param game The game to register players in.
     * @return true if players were successfully registered, false otherwise.
     */
    bool registerPlayers(Game& game) const;
    /**
     * @brief Builds a MazeWrapper object using the loaded maze data.
     * @return A MazeWrapper object representing the loaded maze.
     */
    MazeWrapper buildMaze() const;
    /**
     * @brief Gets the game constants for the maze.
     * @return The game constants for the maze.
     */
    GameConstants getConstants() const;
};