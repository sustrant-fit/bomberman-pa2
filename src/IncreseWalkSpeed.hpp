#pragma once
#include "PowerUp.hpp"
/**
 * @brief A class representing a power-up that increases the walk speed of a player in a game.
 */
class IncreseWalkSpeed : public PowerUp {
   public:
    IncreseWalkSpeed(LocationData location, Game& game);

    void applyEfect(PlayerStats& player) const override;
};
