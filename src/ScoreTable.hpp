#pragma once
#include <map>
#include <string>

/**
 * @brief A class representing a score table.
 */
class ScoreTable {
    bool validData;                              ///< Flag indicating if the score data is valid.
    std::multimap<int, std::string> mScoreData;  ///< Score data stored as a multimap.

   public:
    /**
     * @brief Constructs a ScoreTable object.
     */
    ScoreTable();
    /**
     * @brief Loads score data from the given file.
     * @param fileName The name of the file to load data from.
     */
    void loadData(const std::string& fileName);
    /**
     * @brief Inserts a player with the given name and score into the score table.
     * @param name The name of the player.
     * @param score The score of the player.
     */
    void insertPlayer(const std::string& name, int score);
    /**
     * @brief Saves the score data to the given file.
     * @param fileName The name of the file to save data to.
     */
    void saveData(const std::string& fileName) const;
    /**
     * @brief Displays the score table on the screen.
     */
    void displayScoreTable() const;
};