#pragma once
#include <string>

/**
 * @brief A class representing the main application for a game.
 */
class App {
   public:
    /**
     * @brief Runs the game using the given maze and score file names.
     * @param mazeFileName The name of the file containing the maze data.
     * @param scoreFileName The name of the file containing the score data.
     * @return The exit code of the game.
     */
    int run(const std::string& mazeFileName, const std::string& scoreFileName) const;
};