#pragma once
#include "PowerUp.hpp"
/**
 * @brief A class representing a power-up that increases the num of lives player has in a game.
 */
class IncreseLives : public PowerUp {
   public:
    IncreseLives(LocationData location, Game& game);

    void applyEfect(PlayerStats& player) const override;
};
