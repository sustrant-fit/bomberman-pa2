#include "Bomb.hpp"

#include <sys/time.h>

#include <iostream>

#include "GameConstants.hpp"
#include "IncreseDamage.hpp"
#include "IncreseExplosionSize.hpp"
#include "IncreseLives.hpp"
#include "IncreseNumOfBombs.hpp"
#include "IncreseWalkSpeed.hpp"

Bomb::Bomb(Game& game, LocationData location, int explosionSize, int damage)
    : GameObject("@", location),
      mExploded(false),
      mGame(game),
      mTimeUntilExplosion(game.getGameConstants().getBombTimerInSeconds()),
      mDamage(damage),
      mExplosionSize(explosionSize) {}

void Bomb::update() {
    if (mExploded) {
        mGame.removeObject(shared_from_this());
        return;
    }
    mTimeUntilExplosion--;
    if (mTimeUntilExplosion <= 0) {
        mExploded = true;
    }
}

void drawExplosionLine(LocationData location, size_t direction) {
    std::cout << "\u001b[" << location.mRow << ";" << location.mColumn << "H";
    if (direction % 2 == 0) {
        std::cout << "|" << std::endl;
    } else {
        std::cout << "-" << std::endl;
    }
}

void dropPowerUp(LocationData location, Game& game) {
    // source: https://stackoverflow.com/questions/322938/recommended-way-to-initialize-srand
    // better than normal, because i need different power up for each wall
    struct timeval time;
    gettimeofday(&time, nullptr);
    srand(time.tv_usec);
    int random_variable = std::rand();
    if (random_variable % 100 < game.getGameConstants().getChanceOfBombDamageIncrese()) {
        game.registerGameObject(std::make_shared<IncreseDamage>(location, game));
    } else if (random_variable % 100 <
               game.getGameConstants().getChanceOfBombDamageIncrese() + game.getGameConstants().getChanceOfBombExplosionSizeIncrese()) {
        game.registerGameObject(std::make_shared<IncreseExplosionSize>(location, game));
    } else if (random_variable % 100 < game.getGameConstants().getChanceOfBombDamageIncrese() +
                                           game.getGameConstants().getChanceOfBombExplosionSizeIncrese() +
                                           game.getGameConstants().getChanceOfLiveIncrese()) {
        game.registerGameObject(std::make_shared<IncreseLives>(location, game));
    } else if (random_variable % 100 <
               game.getGameConstants().getChanceOfBombDamageIncrese() + game.getGameConstants().getChanceOfBombExplosionSizeIncrese() +
                   game.getGameConstants().getChanceOfLiveIncrese() + game.getGameConstants().getChanceOfBombNumberIncrese()) {
        game.registerGameObject(std::make_shared<IncreseNumOfBombs>(location, game));
    } else if (random_variable % 100 < game.getGameConstants().getChanceOfBombDamageIncrese() +
                                           game.getGameConstants().getChanceOfBombExplosionSizeIncrese() +
                                           game.getGameConstants().getChanceOfLiveIncrese() + game.getGameConstants().getChanceOfBombNumberIncrese() +
                                           game.getGameConstants().getChanceOfWalkSpeedIncrese()) {
        game.registerGameObject(std::make_shared<IncreseWalkSpeed>(location, game));
    }
}

void Bomb::explode() const {
    LocationData directions[4] = {{0, -1}, {-1, 0}, {0, 1}, {1, 0}};
    for (size_t i = 0; i < 4; i++) {
        LocationData drawLocation = mLocation;
        for (int j = 0; j < mExplosionSize; j++) {
            drawLocation.mColumn += directions[i].mColumn;
            drawLocation.mRow += directions[i].mRow;
            if (mGame.getMaze().canGoHere(drawLocation)) {
                drawExplosionLine(drawLocation, i);
            } else {
                if (mGame.getMaze().canDestroy(drawLocation)) {
                    mGame.getMaze().destroy(drawLocation);
                    dropPowerUp(drawLocation, mGame);
                    drawExplosionLine(drawLocation, i);
                }
                break;
            }
        }
    }
}

void Bomb::draw() const {
    if (!mExploded) {
        GameObject::draw();
    } else {
        std::cout << "\u001b[1m\u001b[32m";
        explode();
        GameObject::draw();
    }
}

void Bomb::checkCollisonWithPlayer(LocationData playerLocation, PlayerStats& player) {
    if (mExploded) {
        if (playerLocation == mLocation) {
            player.mNumOfLives = player.mNumOfLives - mDamage;
            return;
        }
        LocationData directions[4] = {{0, -1}, {-1, 0}, {0, 1}, {1, 0}};
        for (auto& direction : directions) {
            LocationData checkLocation = mLocation;
            for (int j = 0; j < mExplosionSize; j++) {
                checkLocation.mColumn += direction.mColumn;
                checkLocation.mRow += direction.mRow;
                if (mGame.getMaze().canGoHere(checkLocation)) {
                    if (playerLocation == checkLocation) {
                        player.mNumOfLives = player.mNumOfLives - mDamage;
                        return;
                    }
                } else {
                    break;
                }
            }
        }
    }
}