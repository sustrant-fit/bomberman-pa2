#pragma once
#include "PowerUp.hpp"
/**
 * @brief A class representing a power-up that increases the num of available bombs player has in a game.
 */
class IncreseNumOfBombs : public PowerUp {
   public:
    IncreseNumOfBombs(LocationData location, Game& game);

    void applyEfect(PlayerStats& player) const override;
};
