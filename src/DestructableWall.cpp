#include "DestructableWall.hpp"

DestructableWall::DestructableWall() : Tile("\u001b[38;5;238m+") {}

bool DestructableWall::isWalkable() const { return false; }
bool DestructableWall::isDestructable() const { return true; }