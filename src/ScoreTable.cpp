#include "ScoreTable.hpp"

#include <fstream>
#include <iostream>

#include "MalformedDataException.hpp"

ScoreTable::ScoreTable() : validData(false) {}

void ScoreTable::loadData(const std::string& fileName) {
    std::ifstream inStream(fileName, std::ifstream::in);
    if (!inStream.is_open()) {
        throw MalformedDataException("Failed to open score file");
    }

    while (!inStream.eof()) {
        std::string playerName;
        int playerScore;
        inStream >> playerName;
        if (inStream.eof()) {
            break;
        } else if (inStream.bad() || playerName.empty()) {
            throw MalformedDataException("Failed to load player name");
        }

        inStream >> playerScore;
        if (inStream.bad()) {
            throw MalformedDataException("Failed to load player score");
        }
        mScoreData.emplace(playerScore, playerName);
    }
    validData = true;
}
void ScoreTable::insertPlayer(const std::string& name, int score) { mScoreData.emplace(score, name); }
void ScoreTable::saveData(const std::string& fileName) const {
    std::ofstream outStream(fileName, std::ofstream::out | std::ofstream::trunc);
    if (!outStream.is_open()) {
        throw MalformedDataException("Failed to open score file");
    }
    for (auto i = mScoreData.rbegin(); i != mScoreData.rend(); i++) {
        outStream << i->second << "\n" << i->first << std::endl;
    }
}

void ScoreTable::displayScoreTable() const {
    if (!validData) {
        throw MalformedDataException("Data not yet loaded");
    }
    for (auto i = mScoreData.rbegin(); i != mScoreData.rend(); i++) {
        std::cout << i->second << ": " << i->first << std::endl;
    }
}