#pragma once
/**
 * @brief A class representing a location on a game board.
 */
class LocationData {
   public:
    /**
     * @brief Constructs a LocationData object with the given column and row.
     * @param column The column of the location on the game board.
     * @param row The row of the location on the game board.
     */
    LocationData(int column, int row) : mColumn(column), mRow(row) {}
    int mColumn;  ///< The column of the location on the game board.
    int mRow;     ///< The row of the location on the game board.
    /**
     * @brief Checks if this location is equal to another location.
     * @param other The other location to compare to.
     * @return true if this location is equal to the other location, false otherwise.
     */
    inline bool operator==(const LocationData& other) const { return mColumn == other.mColumn && mRow == other.mRow; }
    /**
     * @brief Checks if this location is not equal to another location.
     * @param other The other location to compare to.
     * @return true if this location is not equal to the other location, false otherwise.
     */
    inline bool operator!=(const LocationData& other) const { return !(*this == other); }
};
