#include "MazeWrapper.hpp"

#include <iostream>

#include "DestructableWall.hpp"
#include "FreeTile.hpp"
#include "Wall.hpp"

MazeWrapper::MazeWrapper(std::vector<std::vector<std::shared_ptr<Tile>>> mazeData) : mMazeData(mazeData) {}

bool MazeWrapper::checkLocation(LocationData location) const {
    if (location.mColumn < 0 || location.mRow < 0) {
        return false;
    }
    size_t column = location.mColumn;
    size_t row = location.mRow;

    if (column > mMazeData.size()) {
        return false;
    }
    if (row > mMazeData.at(column - 1).size()) {
        return false;
    }
    return true;
}

bool MazeWrapper::canGoHere(LocationData location) const {
    // terminal is indexed from 1:1
    return checkLocation(location) && mMazeData[location.mRow - 1][location.mColumn - 1]->isWalkable();
}
bool MazeWrapper::canDestroy(LocationData location) const {
    // terminal is indexed from 1:1
    return checkLocation(location) && mMazeData[location.mRow - 1][location.mColumn - 1]->isDestructable();
}

bool MazeWrapper::destroy(LocationData location) {
    if (!checkLocation(location) || !canDestroy(location)) {
        return false;
    }
    // terminal is indexed from 1:1
    mMazeData[location.mRow - 1][location.mColumn - 1] = std::make_shared<FreeTile>();
    return true;
}

void MazeWrapper::render() const {
    std::cout << "\u001b[2J"
              << "\u001b[H";
    for (auto &&row : mMazeData) {
        for (auto &&tile : row) {
            tile->draw();
        }
        std::cout << std::endl;
    }
}