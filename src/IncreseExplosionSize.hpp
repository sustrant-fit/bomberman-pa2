#pragma once
#include "PowerUp.hpp"
/**
 * @brief A class representing a power-up that increases the explosion size of bombs player has in a game.
 */
class IncreseExplosionSize : public PowerUp {
   public:
    IncreseExplosionSize(LocationData location, Game& game);

    void applyEfect(PlayerStats& player) const override;
};
