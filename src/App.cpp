#include "App.hpp"

#include <unistd.h>

#include "Game.hpp"
#include "MazeLoader.hpp"
#include "ScoreTable.hpp"

int App::run(const std::string& mazeFileName, const std::string& scoreFileName) const {
    if (isatty(STDOUT_FILENO)) {
        std::cout << "\u001b[2J"
                  << "\u001b[H";
        std::cout << "Check if you can see correctly next symbols (y/n):\n"
                  << "\u001b[38;5;238m+\u001b[0m (grey plus)\n"
                  << "\u001b[1m\u001b[32m|,-,@ \u001b[0m (green pipe, minus and at sign)\n"
                  << "\u001b[1m#\u001b[0m (bold hash)" << std::endl;
        char userInput;
        std::cin >> userInput;
        if (userInput == 'n' || userInput == 'N') {
            std::cout << "Sorry game is playable only on color supporting terminals" << std::endl;
            return -1;
        } else if (userInput != 'y' && userInput != 'Y') {
            std::cout << "Wrong input" << std::endl;
            return -1;
        }

        try {
            MazeLoader mazeLoader;
            mazeLoader.loadData(mazeFileName);
            ScoreTable scoreTable;
            scoreTable.loadData(scoreFileName);
            Game game(mazeLoader.buildMaze(), mazeLoader.getConstants(), scoreTable);
            mazeLoader.registerPlayers(game);
            game.run();
            scoreTable.saveData(scoreFileName);
        } catch (const std::exception& e) {
            std::cerr << e.what() << '\n';
        }
        return 0;

    } else {
        std::cout << "Output is not terminal. Game is interactive and is required to start it from terminal." << std::endl;
        return -1;
    }
}