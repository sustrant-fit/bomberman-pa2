#include "IncreseDamage.hpp"

IncreseDamage::IncreseDamage(LocationData location, Game& game) : PowerUp(location, "d", game) {}

void IncreseDamage::applyEfect(PlayerStats& player) const { player.mBombDamage++; }