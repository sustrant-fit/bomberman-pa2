#pragma once
#include <exception>

/**
 * @brief A class representing an exception thrown when there are no more players alive in a game.
 */
class NoPlayersException : public std::exception {
   private:
    std::string mMessage;  ///< The message associated with the exception.

   public:
    /**
     * @brief Constructs a NoPlayersException object with a default message.
     */
    NoPlayersException() : mMessage("Game over, no more players alive") {}
    /**
     * @brief Constructs a NoPlayersException object with the given message.
     * @param message The message associated with the exception.
     */
    NoPlayersException(std::string message) : mMessage(message) {}
    /**
     * @brief Gets the message associated with the exception.
     * @return The message associated with the exception.
     */
    const char* what() const noexcept override { return mMessage.c_str(); }
};