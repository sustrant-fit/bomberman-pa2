#pragma once
/**
 * @brief A class representing the game constants for a game.
 */
class GameConstants {
   private:
    bool validData;  ///< Flag indicating if the game constants data is valid.
                     /**
                      * @brief Checks if the game constants data is valid.
                      */
    void checkValid() const;
    int mNumOfColumns;                      ///< The number of columns in the game board.
    int mNumOfRows;                         ///< The number of rows in the game board.
    int mBombTimerInSeconds;                ///< The timer for bombs in seconds.
    int mChanceOfLiveIncrese;               ///< The chance of a power-up increasing the number of lives for a player.
    int mChanceOfBombDamageIncrese;         ///< The chance of a power-up increasing the damage of bombs for a player.
    int mChanceOfWalkSpeedIncrese;          ///< The chance of a power-up increasing the walk speed for a player.
    int mChanceOfBombNumberIncrese;         ///< The chance of a power-up increasing the number of bombs for a player.
    int mChanceOfBombExplosionSizeIncrese;  ///< The chance of a power-up increasing the explosion size of bombs for a player.

   public:
    /**
     * @brief Constructs a GameConstants object with default values.
     */
    GameConstants();
    /**
     * @brief Constructs a GameConstants object with the given values.
     * @param numOfColumns The number of columns in the game board.
     * @param numOfRows The number of rows in the game board.
     * @param bombTimerInSeconds The timer for bombs in seconds.
     * @param chanceOfLiveIncrease The chance of a power-up increasing the number of lives for a player.
     * @param chanceOfBombDamageIncrease The chance of a power-up increasing the damage of bombs for a player.
     * @param chanceOfWalkSpeedIncrease The chance of a power-up increasing the walk speed for a player.
     * @param chanceOfBombNumberIncrease The chance of a power-up increasing the number of bombs for a player.
     * @param chanceOfBombExplosionSizeIncrease The chance of a power-up increasing the explosion size of bombs for a player.
     */
    GameConstants(int numOfColumns, int numOfRows, int bombTimerInSeconds, int chanceOfLiveIncrese, int chanceOfBombDamageIncrese,
                  int chanceOfWalkSpeedIncrese, int chanceOfBombNumberIncrese, int chanceOfBombExplosionSizeIncrese);
    /**
     * @brief Gets the number of columns in the game board.
     * @return The number of columns in the game board.
     */
    int getNumOfColumns() const;
    /**
     * @brief Gets the number of rows in the game board.
     * @return The number of rows in the game board.
     */
    int getNumOfRows() const;
    /**
     * @brief Gets the timer for bombs in seconds.
     * @return The timer for bombs in seconds.
     */
    int getBombTimerInSeconds() const;
    /**
     * @brief Gets the chance of a power-up increasing the number of lives for a player.
     * @return The chance of a power-up increasing the number of lives for a player.
     */
    int getChanceOfLiveIncrese() const;
    /**
     * @brief Gets the chance of a power-up increasing the damage of bombs for a player.
     * @return The chance of a power-up increasing the damage of bombs for a player.
     */
    int getChanceOfBombDamageIncrese() const;
    /**
     * @brief Gets the chance of a power-up increasing the walk speed for a player.
     * @return The chance of a power-up increasing the walk speed for a player.
     */
    int getChanceOfWalkSpeedIncrese() const;
    /**
     * @brief Gets the chance of a power-up increasing the number of bombs for a player.
     * @return The chance of a power-up increasing the number of bombs for a player.
     */
    int getChanceOfBombNumberIncrese() const;
    /**
     * @brief Gets the chance of a power-up increasing the explosion size of bombs for a player.
     * @return The chance of a power-up increasing the explosion size of bombs for a player.
     */
    int getChanceOfBombExplosionSizeIncrese() const;
};