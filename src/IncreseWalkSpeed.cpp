#include "IncreseWalkSpeed.hpp"

IncreseWalkSpeed::IncreseWalkSpeed(LocationData location, Game& game) : PowerUp(location, "S", game) {}

void IncreseWalkSpeed::applyEfect(PlayerStats& player) const { player.mWalkSpeedMultiplier++; }