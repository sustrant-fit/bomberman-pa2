#pragma once

#include <string>
/**
 * @brief A class representing a tile on a game board.
 */
class Tile {
    std::string mTextRepresentation;  ///< The text representation of the tile.

   public:
    /**
     * @brief Constructs a Tile object with the given text representation.
     * @param textRepresentation Text representation of the tile.
     */
    Tile(const std::string& textRepresentation) : mTextRepresentation(textRepresentation) {}
    Tile(const Tile&) = delete;
    Tile& operator=(const Tile&) = delete;
    virtual ~Tile() = default;
    /**
     * @brief Checks if the tile is walkable.
     * @return true if the tile is walkable, false otherwise.
     */
    virtual bool isWalkable() const = 0;
    /**
     * @brief Checks if the tile is destructable.
     * @return true if the tile is destructable, false otherwise.
     */
    virtual bool isDestructable() const = 0;
    /**
     * @brief Draws the tile on the game board.
     */
    void draw() const;
};