#pragma once
#include <memory>
#include <set>

#include "GameConstants.hpp"
#include "GameObject.hpp"
#include "InputHandler.hpp"
#include "KeyboardControls.hpp"
#include "MazeWrapper.hpp"
#include "ScoreTable.hpp"
class Player;
/**
 * @brief A class representing a game.
 */
class Game {
   public:
    /**
     * @brief Constructs a Game object with the given maze, game constants, and score table.
     * @param maze The maze for the game.
     * @param constants The game constants for the game.
     * @param scoreTable The score table for the game.
     */
    Game(MazeWrapper maze, GameConstants constants, ScoreTable& scoreTable);
    /**
     * @brief Gets a reference to the maze for the game.
     * @return A reference to the maze for the game.
     */
    MazeWrapper& getMaze();
    /**
     * @brief Gets a reference to the game constants for the game.
     * @return A reference to the game constants for the game.
     */
    GameConstants& getGameConstants();
    /**
     * @brief Gets a reference to the input handler for the game.
     * @return A reference to the input handler for the game.
     */
    InputHandler& getInputHandler();
    /**
     * @brief Registers a game object in the game.
     * @param toRegister The game object to register in the game.
     */
    void registerGameObject(const std::shared_ptr<GameObject>& toRegister);
    /**
     * @brief Registers a player in the game.
     * @param toRegister The player to register in the game.
     */
    void registerPlayer(const std::shared_ptr<Player>& toRegister);
    /**
     * @brief Runs the game loop until the game is over.
     */
    void run();
    /**
     * @brief Removes a game object from the game.
     * @param toRemove The game object to remove from the game.
     */
    void removeObject(const std::shared_ptr<GameObject>& toRemove);
    /**
     * @brief Removes a player from the game.
     * @param toRemove The player to remove from the game.
     */
    void removePlayer(const std::shared_ptr<Player>& toRemove);

   private:
    InputHandler mInputHandler;                          ///< The input handler for the game.
    MazeWrapper mMaze;                                   ///< The maze for the game.
    GameConstants mConstants;                            ///< The game constants for the game.
    ScoreTable& mScoreTable;                             ///< The score table for the game.
    std::set<std::shared_ptr<GameObject>> mGameObjects;  ///< The set of game objects in the game.
    std::set<std::shared_ptr<Player>> mPlayers;          ///< The set of players in the game.
    /**
     * @brief Processes collisions between game objects and players in the game.
     */
    void processColisions();
    /**
     * @brief Renders the game state on the screen.
     */
    void render() const;
    /**
     * @brief Updates the state of the game and its objects.
     */
    void update();
};
