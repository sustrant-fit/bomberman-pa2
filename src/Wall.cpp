#include "Wall.hpp"

Wall::Wall() : Tile("\u001b[1m#") {}

bool Wall::isWalkable() const { return false; }
bool Wall::isDestructable() const { return false; }