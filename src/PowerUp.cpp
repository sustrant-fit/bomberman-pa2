#include "PowerUp.hpp"

void PowerUp::checkCollisonWithPlayer(LocationData location, PlayerStats& player) {
    if (location == mLocation) {
        applyEfect(player);
        mGame.removeObject(shared_from_this());
    }
}