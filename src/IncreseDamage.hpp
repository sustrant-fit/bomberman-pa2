#pragma once
#include "PowerUp.hpp"
/**
 * @brief A class representing a power-up that increases the damage of bombs player has in a game.
 */
class IncreseDamage : public PowerUp {
   public:
    IncreseDamage(LocationData location, Game& game);

    void applyEfect(PlayerStats& player) const override;
};
