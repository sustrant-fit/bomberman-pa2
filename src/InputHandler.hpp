#pragma once
#include <functional>
#include <iostream>
#include <map>
#include <string>

#include "KeyboardControls.hpp"
#include "LocationData.hpp"
/**
 * @brief A class representing an input handler for a game.
 */
class InputHandler {
   private:
    std::map<std::string, std::map<std::string, std::function<void()>>> keyBindings;  ///< The key bindings for the players.

   public:
    /**
     * @brief Constructs an InputHandler object.
     */
    InputHandler() = default;
    InputHandler(const InputHandler&) = delete;
    InputHandler& operator=(const InputHandler&) = delete;
    /**
     * @brief Handles input from the given input stream and location.
     * @param input The input stream to read input from.
     * @param inputLocation The location of the input on the game board.
     */
    void handleInput(std::istream& input, LocationData inputLocation) const;
    /**
     * @brief Registers the given key bindings and functions for the given player.
     * @param playerName The name of the player to register key bindings for.
     * @param keys The key bindings for the player.
     * @param upFunctionToCall The function to call when the player presses the up key.
     * @param downFunctionToCall The function to call when the player presses the down key.
     * @param leftFunctionToCall The function to call when the player presses the left key.
     * @param rightFunctionToCall The function to call when the player presses the right key.
     * @param placeBombFunctionToCall The function to call when the player presses the place bomb key.
     */
    void registerInput(std::string playerName, KeyboardControls keys, std::function<void()> upFunctionToCall,
                       std::function<void()> downFunctionToCall, std::function<void()> leftFunctionToCall, std::function<void()> rightFunctionToCall,
                       std::function<void()> placeBombFunctionToCall);
    /**
     * @brief Tries to remove the key bindings for the given player.
     * @param playerName The name of the player to remove key bindings for.
     */
    void tryRemoveInput(const std::string& playerName);
};
