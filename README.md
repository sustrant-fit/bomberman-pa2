# Bomberman-PA2

Export of semestral work to show of bigger project in C++. Because the assignment description cannot be shown to public.

## Usage

Build project using make:

```make```

Start the game by ./sustrant [Maze file to load] [File to save score to]. For example:

```./sustrant examples/fullGame.txt examples/ScoreTable.txt```

In default configuration is player movement controlled by wsad and f to place bomb. Each steps needs to be confirmed by pressing Enter in default configuration. There is a place for improvement.

## Doxygen documentation

```make doc```

Index.html is located in doc/index.html

Note: Most of the comments are generated because of deadline in exam season

## Maze configuration file

Each value needs new line (see examples):

1. Number of columns
2. Number of rows
3. Maze with walls around (# is wall, . is free space and + is destructable wall)
4. Bomb timer
5. 5 lines of chance to drop an power up (sum needs to be <= 100)
6. Player lines, number of players is not limited:
   1. Name of player to write in score board
   2. Type of player: 
      1. H for human player
      2. A for AI player
   3. Location of player:
      1. First number is column
      2. Second number is row
   4. (If player type is human) keys for control:
      1. up
      2. down
      3. left
      4. right
      5. bomb placement