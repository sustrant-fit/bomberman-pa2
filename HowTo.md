# Bomberman by sustrant

Here is quick introduction of the game in base config.

Player is located in top left corner, his available moves are:

- w to move up
- s to move down
- a to move left
- d to move right
- f to place bomb

Default bomb timer is 5 seconds.