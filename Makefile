TARGET=sustrant
OBJECT_DIR = build
SOURCE_DIR = ./src/

CXX=g++
LD=g++
CXXFLAGS=-std=c++17 -Wall -pedantic -g -fsanitize=address
LDFLAGS=-fsanitize=address

MAZEFILENAME=./examples/fullGame.txt
SCOREFILENAME=./examples/ScoreTable.txt

SOURCES := $(wildcard $(SOURCE_DIR)/*.cpp)
OBJECTS := $(patsubst $(SOURCE_DIR)/%.cpp,$(OBJECT_DIR)/%.o,$(SOURCES))

all: $(TARGET) doc

compile: $(TARGET)

$(OBJECT_DIR)/%.o : $(SOURCE_DIR)/%.cpp
	mkdir -p build
	$(CXX) $(CXXFLAGS) -MMD -c $< -o $@

$(TARGET): $(OBJECTS)
	$(LD) $(LDFLAGS) $^ -o $@

doc:
	doxygen Doxyfile

clean:
	rm -rf $(OBJECT_DIR)/
	rm -f $(TARGET)
	rm -rf doc/

run : $(TARGET)
	./$(TARGET) $(MAZEFILENAME) $(SCOREFILENAME)

.PHONY: clean all run
